/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.4.24-MariaDB : Database - apparel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`apparel` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `apparel`;

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role_name`) values (1,'Admin'),(2,'Staff'),(3,'User'),(8,'cc');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `image_address` varchar(255) DEFAULT NULL,
  `status` int(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`last_name`,`email`,`password`,`role_id`,`image_address`,`status`,`created_at`) values (25,'admin','admin','admin','admin',1,NULL,1,NULL),(26,'Holly','Pickett','nawypefy@mailinator.com','In tenetur enim aut ',2,NULL,1,NULL),(27,'Blythe','Ball','cedewix@mailinator.com','Officia dicta reicie',2,NULL,1,NULL),(28,'Nigel','Graves','peruwoh@mailinator.com','Error hic libero dol',2,NULL,1,NULL),(29,'Sara','Ayers','bojav@mailinator.com','Quis ex veniam elit',2,NULL,1,NULL),(30,'Ora','Santana','moxivybet@mailinator.com','Sint error qui aliqu',2,NULL,1,NULL),(31,'Cora','Ball','jukanuzom@mailinator.com','Rerum ea sint qui s',2,NULL,1,NULL),(32,'Keegan','Fowler','kexibe@mailinator.com','Voluptas aut lorem e',2,NULL,1,NULL),(33,'Teagan','Clarke','fobuhahan@mailinator.com','Error quis aut sint',2,NULL,1,NULL),(34,'Chava','Hogan','jaduvyva@mailinator.com','Dolore sed proident',2,NULL,1,NULL),(35,'Briar','Mathews','pyfijaqovu@mailinator.com','Rerum dolorem quidem',2,NULL,1,NULL),(36,'Karleigh','Hewitt','rozasivib@mailinator.com','Dolorem veritatis pa',2,NULL,1,NULL),(37,'Shay','Hogan','lawoqype@mailinator.com','Non voluptates nisi ',2,NULL,1,NULL),(38,'Rahim','Strickland','mijyjuv@mailinator.com','Quidem fugiat conseq',2,NULL,1,NULL),(39,'Gareth','Mccormick','hepup@mailinator.com','Debitis cupidatat nu',2,NULL,1,NULL),(40,'Ria','Armstrong','lycipu@mailinator.com','Quos expedita rem ut',2,NULL,1,NULL),(41,'Irene','Sandoval','cyjesade@mailinator.com','Esse voluptas digni',2,NULL,1,NULL),(42,'Madeline','Glenn','riqaqytid@mailinator.com','Et enim quae id nesc',2,NULL,1,NULL),(43,'Rahim','Vasquez','kevis@mailinator.com','Cillum quia possimus',2,NULL,1,NULL),(44,'Reuben','Blair','mudydypovo@mailinator.com','Debitis eveniet ull',2,NULL,1,NULL),(45,'Wynter','Patrick','rosob@mailinator.com','Ex tempora nostrum c',2,NULL,1,NULL),(46,'Rhea','Fitzgerald','bohizevoc@mailinator.com','Quia eaque esse aliq',8,NULL,1,NULL),(47,'Sybill','Scott','hasumyvo@mailinator.com','Voluptate proident ',2,NULL,1,NULL),(48,'Ulysses','Weaver','diwyq@mailinator.com','In facere consequat',8,NULL,1,NULL),(51,'Reese','Gregory','rorymaka@mailinator.com','Eum reiciendis est N',3,'Publication Picture.PNG',1,NULL),(52,'Jaime','Ferrell','todelaxina@mailinator.com','Animi ratione esse ',1,'products_picture.PNG',1,NULL),(53,'Brynn','Hawkins','nebyj@mailinator.com','Accusantium esse ut',8,'products_picture.PNG',1,NULL),(54,'Plato','Bender','wesud@mailinator.com','Similique qui accusa',3,'2022-09-24Publication Picture.PNG',1,NULL),(55,'Kasper','Nixon','vybawu@mailinator.com','Aut id adipisci cons',2,'2022-09-24products_picture.PNG',1,NULL),(56,'','','','',0,'2022-09-24products_picture.PNG',1,NULL),(57,'Jameson','Hancock','velehup@mailinator.com','Pariatur Corporis o',1,'2022-09-24Publication Picture.PNG',1,NULL),(58,'Linda','Morris','sywoseta@mailinator.com','Dolor pariatur Exce',1,'../Images/UserImages/Publication Picture.PNG',1,NULL),(59,'Ronan','Bond','lyhom@mailinator.com','Totam tempore et ve',1,'../Images/UserImages/Publication Picture.PNG',1,NULL),(60,'Gloria','Boone','jucoly@mailinator.com','Sequi rerum quos odi',8,'../Images/UserImages/Publication Picture.PNG',1,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
