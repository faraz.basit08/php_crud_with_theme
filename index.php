<?php
    session_start();
    if(!isset($_SESSION['username']))
    {
      header('location: Login/login.php');
    }

    include('Db_Connectivity/connection.php');
    include('master_layoout.php');

    // Fetching
    $query = "select * from users ORDER by id DESC ";
    $result = mysqli_query($conn,$query);
    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);

?>

  <a href="insert.php" class="btn btn-secondary">Add New</a>
  <a href="../logout.php" class="btn btn-secondary">Logout</a>

  <table class="table table-hover ">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Email</th>
      <th scope="col">Password</th>
      <th scope="col" colspan="3">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php 
      for($i = 0; $i<count($data); $i++)
      {
        ?>
        <tr>
          <td scope="row"><?php echo $i+1;?></td>
          <td scope="row"><?php echo $data[$i]['first_name']?></td>
          <td scope="row"><?php echo $data[$i]['last_name']?></td>
          <td scope="row"><?php echo $data[$i]['email']?></td>
          <td scope="row"><?php echo $data[$i]['password']?></td>
        
          <td><a href="update.php?id=<?= $data[$i]['id']?>" class="btn btn-success" >Edit</a> </td>
          <!-- <td><a href="delete.php?id=<?= $data[$i]['id']?>" class="btn btn-danger" >Delete</a> </td> -->
          <td><button onclick="deleteUser(<?= $data[$i]['id']?>)" class="btn btn-danger" >Delete</button> </td>
          <td><a href="view.php?id=<?= $data[$i]['id']?>" class="btn btn-primary" >View</a> </td>
        </tr>
     <?php
      }
    ?>

      
      <!-- <td>
        <?php 
            // if(isset($formData['firstname']))
            // {
            //     echo $formData['firstname'];
            // }
        ?>
      </td>
      <td>
        <?php 
            // if(isset($formData['lastname']))
            // {
            //     echo $formData['lastname'];
            // }
        ?>
      </td>
      <td><?= $formData['emailaddress'] ?? ''?></td>
      <td><?= $formData['password'] ?? ''?></td> -->
    <!-- </tr> -->
  </tbody>
</table>
<script>
function deleteUser(id)
{
  debugger
  event.preventDefault(); // prevent form submit
  var form = event.target.form; // storing the form
          swal({
    title: "Are you sure?",
    text: "But you will still be able to retrieve this file.",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, archive it!",
    cancelButtonText: "No, cancel please!",
    closeOnConfirm: false,
    closeOnCancel: false
  },
  function(isConfirm){
    if (isConfirm) {
      form.submit();          // submitting the form when user press yes
    } else {
      swal("Cancelled", "Your imaginary file is safe :)", "error");
    }
  });

}
</script>


 <?php 
  include('footer.php');
 ?>   
