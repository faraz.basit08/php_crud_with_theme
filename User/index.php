<?php
    session_start();
    
    if(!isset($_SESSION['username']))
    {
      header('location: ../Login/login.php');
    }

    include('../Db_Connectivity/connection.php');
    include('../master_layoout.php');

    // Fetching
    $query = "
	SELECT 
	u.`id`,
	u.`first_name`,
	u.`last_name`,
	u.`email`,
	u.`password`,
	r.`role_name` 
	FROM
	users u 
	INNER JOIN roles r 
		ON u.`role_id` = r.`id` 
	WHERE STATUS <> 2 
	ORDER BY id DESC ;" ;

    $result = mysqli_query($conn,$query);
    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);

?>


<div class="row">
	<div class="col-xs-12">
		<h3 class="header smaller lighter blue"><a href="insert.php" style="background-color: #307ecc !important; border: none;" class="btn btn-default">Add New</a></h3>
		<div class="table-header">
			Users List
		</div>

		<div class="table-responsive">
			<table id="sample-table-2" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th class="center">
							<label>
								<input type="checkbox" class="ace" />
								<span class="lbl"></span>
							</label>
						</th>
						<th>#</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th class="">Email</th>

						<th>
							<!-- <i class="icon-time bigger-110 hidden-480"></i> -->
							Role
						</th>
						<th class="">Status</th>

						<th></th>
					</tr>
				</thead>
				<tbody>
				<?php 
				for($i = 0; $i<count($data); $i++)
				{
				?>
					<tr>
						<td class="center">
							<label>
								<input type="checkbox" class="ace" />
								<span class="lbl"></span>
							</label>
						</td>

						<td>
							<a href="#"><?php echo $i+1;?></a>
						</td>
						<td><?php echo $data[$i]['first_name'] ?? '' ?></td>
						<td class="hidden-480"><?php echo $data[$i]['last_name'] ?? '' ?></td>
						<td class="hidden-480"><?php echo $data[$i]['email'] ?? '' ?></td>
						<td class="hidden-480"><?php echo $data[$i]['role_name'] ?? '' ?></td>
						<td class="hidden-480">
							<span class="label label-sm label-warning">Expiring</span>
						</td>

						<td>
							<div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
								<a class="blue" href="view.php?id=<?= $data[$i]['id']?>">
									<i class="icon-eye-open bigger-130"></i>
								</a>

								<a class="green" href="update.php?id=<?= $data[$i]['id']?>">
									<i class="icon-pencil bigger-130"></i>
								</a>

								<a class="red" href="" onclick="deleteUser(<?= $data[$i]['id']?>)">
									<i class="icon-trash bigger-130"></i>
								</a>
							</div>

							<div class="visible-xs visible-sm hidden-md hidden-lg">
								<div class="inline position-relative">
									<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
										<i class="icon-caret-down icon-only bigger-120"></i>
									</button>

									<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
										<li>
											<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
												<span class="blue">
													<i class="icon-zoom-in bigger-120"></i>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
												<span class="green">
													<i class="icon-edit bigger-120"></i>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
												<span class="red">
													<i class="icon-trash bigger-120"></i>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>
function deleteUser(id)
{
	debugger;
  event.preventDefault(); // prevent form submit
  var form = event.target.form; // storing the form
          swal({
    title: "Are you sure?",
    text: "User will be deleted.",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete role!",
    cancelButtonText: "No",
    closeOnConfirm: false,
    closeOnCancel: false
  },
  function(isConfirm){
    if (isConfirm) {
      var request = $.ajax({
        url: "delete.php",
        type: "GET",
        data: {id : id},
        json: "html"
      });
      request.done(function(msg) {
        if(msg.is_deleted == 1)
        {
          swal("Deleted", "User Deleted", "success");
          location.reload();
        }
        if(msg.is_deleted == 0)
        {
          swal("Not Deleted", "User could not be deleted", "error");
        }
      });

      request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
      });

    } else {
      swal("Cancelled", "User did not deleted", "error");
    }
  });
}
</script>

<?php
    session_abort();
    include('../footer.php');
?>
<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null,null,null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
		</script>
