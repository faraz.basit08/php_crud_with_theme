<?php
ob_start();
session_start();
    
if(!isset($_SESSION['username']))
{
  header('location: ../Login/login.php');
}
 include('../Db_Connectivity/connection.php');
 include('../master_layoout.php');

 // Fetching
$query = "select * from roles ORDER by id DESC ";
$result = mysqli_query($conn,$query);
$data['roles'] = mysqli_fetch_all($result, MYSQLI_ASSOC);

if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
        // Fetching
        $query = "select * from users where id='".$_GET['id']."' ";
        $result = mysqli_query($conn,$query);
        $data['user'] = mysqli_fetch_assoc($result);
    }

?>

<div class="row">
  <div class="col-sm-5">
    <div class="widget-box">
      <div class="widget-header">
        <h4>Create Role</h4>
      </div>
      <div class="widget-body">
        <div class="widget-main no-padding">
          <form  method="POST">
            <!-- <legend>Form</legend> -->

            <fieldset>
              <label>First Name</label>
              <input type="text" class="form-control" value="<?= $data['user']['last_name'] ?? ''?>" id="firstname" name="firstname" aria-describedby="firstname" placeholder="Enter first name" />
              <span class="help-block"></span>

              <label>Last Name</label>
              <input type="text" class="form-control" value="<?= $data['user']['last_name'] ?? ''?>" id="lastname" name="lastname" aria-describedby="lastname" placeholder="Enter last name" />
              <span class="help-block"></span>

              <label>Email</label>
              <input type="text" class="form-control" value="<?= $data['user']['email'] ?? ''?>" id="emailaddress" name="emailaddress" aria-describedby="emailHelp" placeholder="Enter email" />
              <span class="help-block"></span>

              <label>Password</label>
              <input type="text" class="form-control" value="<?= $data['user']['password'] ?? ''?>" id="password" name="password" placeholder="Enter password here"/>
              <span class="help-block"></span>
              <div>
              <label for="form-field-select-3">Role</label>
              <br />
              <select class="chosen-selectt form-control" id="form-field-select-3" data-placeholder="Choose a Country...">
                <option value="">&nbsp;</option>
                <?php 
                foreach ($data['roles'] as $key => $value) {
                  ?>
                  <option value="<?= $value['id']?>"<?=$value['id'] == $data['user']['role_id'] ? ' selected="selected"' : '';?>><?= $value['role_name']?></option>
                <?php 
                  }
                ?>
              </select>
            </div>
            </fieldset>

            
            <div class="form-actions center">
              <button type="submit" class="btn btn-sm btn-success">
                Submit
                <i class="icon-arrow-right icon-on-right bigger-110"></i>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>



<form method="POST">
<div class="container-fluid">
<div class="form-group col-md-6">
    <label for="exampleInputfirstname">first name</label>

    <input type="hidden" class="form-control" id="exampleInputfirstname" name="user_id" value="<?= $data['id'] ?? ''?>" aria-describedby="firstname" placeholder="Enter first name">
    <input type="text" class="form-control" id="exampleInputfirstname" name="firstname" value="<?= $data['user']['first_name'] ?? ''?>" aria-describedby="firstname" placeholder="Enter first name">
    
    <small id="firstname" class="form-text text-muted"></small>
  </div>
  <div class="form-group col-md-6">
    <label for="exampleInputlastname">last name</label>
    <input type="text" class="form-control" id="exampleInputlastname" name="lastname" value="<?= $data['user']['last_name'] ?? ''?>" aria-describedby="lastname" placeholder="Enter last name">
    <small id="lastname" class="form-text text-muted"></small>
  </div>
  <div class="form-group col-md-6">
    <label for="exampleInputEmail">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="emailaddress" value="<?= $data['email'] ?? ''?>" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted"></small>
  </div>
  <div class="form-group col-md-6">
    <label for="exampleInputPassword">Password</label>
    <input type="password" class="form-control" name="password" id="exampleInputPassword1" value="<?php echo $data['password'] ?? ''?>" placeholder="Password">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  </div>
  
</form>
<?php
    $formData = array();    
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {

        $formData = $_POST;
        // $query = "Insert Into users(first_name,last_name,email,password) Values('".$formData['firstname']."',
        // '".$formData['lastname']."','".$formData['emailaddress']."','".$formData['password']."'
        // )";

        $query = "update users set first_name = '".$formData['firstname']."' , last_name = '".$formData['lastname']."',
        email = '".$formData['emailaddress']."' , password = '".$formData['password']."' where id='".$formData['user_id']."' ";

        $isInserted = mysqli_query($conn,$query);
        if($isInserted)
        {
          header("Location: index.php");
        }
        else
        {
          echo "<script>alert('Data could not be inserted')</script>";
        }

      }
?>
 
 <?php
    include('../footer.php');
?>

<script>
  $(".chosen-selectt").chosen(); 
				$('#chosen-multiple-style').on('click', function(e){
					var target = $(e.target).find('input[type=radio]');
					var which = parseInt(target.val());
					if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
					 else $('#form-field-select-4').removeClass('tag-input-style');
				});
</script>