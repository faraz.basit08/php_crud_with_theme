<?php
ob_start();
session_start();
    
if(!isset($_SESSION['username']))
{
  header('location: ../Login/login.php');
}
 include('../Db_Connectivity/connection.php');
 include('../master_layoout.php');

// Fetching
$query = "select * from roles";
$result = mysqli_query($conn,$query);
$data['roles'] = mysqli_fetch_all($result, MYSQLI_ASSOC);
// echo "<pre>";
// print_r($data['roles']);
// die();


?>
<div class="row">
  <div class="col-sm-5">
    <div class="widget-box">
      <div class="widget-header">
        <h4>Create User</h4>
      </div>
      <div class="widget-body">
        <div class="widget-main no-padding">
        <form method="POST" action="" enctype="multipart/form-data">
            <!-- <legend>Form</legend> -->

            <fieldset>
              <label>First Name</label>
              <input type="text" class="form-control" id="firstname" name="firstname" aria-describedby="firstname" placeholder="Enter first name" />
              <?php echo $empty_first_name ?? ''?>

              <label>Last Name</label>
              <input type="text" class="form-control" id="lastname" name="lastname" aria-describedby="lastname" placeholder="Enter last name" />
              <?php echo $empty_last_name ?? ''?>

              <label>Email</label>
              <input type="text" class="form-control" id="emailaddress" name="emailaddress" aria-describedby="emailHelp" placeholder="Enter email" />
              <?= $empty_email ?? ''?>
              <?= $invalid_email ?? ''?>

              <label>Password</label>
              <input type="text" class="form-control" id="password" name="password" placeholder="Enter password here"/>
              <?= $empty_password ?? ''?>
              <?= $password_not_matched ?? ''?>

              <label>Confirm Password</label>
              <input type="text" class="form-control" id="password" name="confirm_password" placeholder="Enter password here"/>
              <?= $empty_confirm_password ?? ''?>
              <?= $password_not_matched ?? ''?>

              <div>
              <label for="form-field-select-3">Role</label>
              <br />
              <select class="chosen-selectt form-control" id="form-field-select-3" name="role_id" data-placeholder="Choose a Role...">
                <option value="">&nbsp;</option>
                <?php 
                foreach ($data['roles'] as $role) {
                  ?>
                  <option value="<?= $role['id']?>"> <?= $role['role_name']?> </option>
                <?php 
                  }
                ?>
              </select>
            </div>
            <?= $empty_role ?? ''?>
            
            <div class="row">
              <div class="col-md-12">
                <div class="widget-box">
                  <label for="form-field-select-3">User Image</label>
                  <div class="widget-header">
                    <h4>Select your image</h4>

                    <span class="widget-toolbar">
                      <a href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                      </a>

                      <a href="#" data-action="close">
                        <i class="icon-remove"></i>
                      </a>
                    </span>
                  </div>

                  <div class="widget-body">
                    <div class="widget-main">
                    <!-- <input type="file" id="myfile" name="myfile"> -->

                    <input type="file" id="id-input-file-2" name="image_address" />
                      <!-- <input multiple="" type="file" id="id-input-file-3" />
                      <label>
                        <input type="checkbox" name="file-format" id="id-file-format" class="ace" />
                        <span class="lbl"> Allow only images</span>
                      </label> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?= $empty_image ?? ''?>
          </fieldset>

            
            <div class="form-actions center">
              <button type="submit" class="btn btn-sm btn-success" name="submitUserData">
                Submit
                <i class="icon-arrow-right icon-on-right bigger-110"></i>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
$data = $_SERVER['REQUEST_METHOD'];
if($data == 'POST')
{
  if(validateData($_POST))
  {
    InsertRecords($data);
  }
}

function validateData($data)
{
  if($data['firstname'] == "") {
    $empty_first_name =  "<span class='error'>Please enter your name.</span>";
    echo $empty_first_name;
  }

  elseif(!isset($data['lastname']) == "") {
    echo "here2";
  $empty_last_name=  "<span class='error'>Please enter your email</span>"; 
  } 

  elseif(!isset($data['emailaddress'])  == "") {
    echo "here3";
  $empty_email =  "<span class='error'>Please enter your email</span>"; 
  } 

  elseif(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", isset($data['emailaddress']))){
    echo "here4";
  $invalid_email= "<span class='error'>Please enter valide email, like your@abc.com</span>";
  }

  // elseif(isset($emp_ph == "")){
  // $empty_ph =  "<span class='error'>Please enter phone number.</span>";
  // }

  // elseif(isset(is_numeric(trim($emp_ph)) == false)){
  // $empty_ph =  "<span class='error'>Please enter numeric value.</span>";
  // }

  // elseif(isset($emp_uname == "")){
  // $empty_username =  "<span class='error'>Please enter uername.</span>";
  // }

  // elseif(isset(strlen($emp_uname)<5)){
  // $empty_username =  "<span class='error'>Username should be atleast five characters.</span>";
  // }

  elseif(!isset($data['password']) == ""){
    echo "here5";
  $empty_password=  "<span class='error'>Please enter password</span>";
  }

  elseif(isset($confirm) == ""){
    echo "here6";
  $empty_confirm=  "<span class='error'>Please enter confirm password</span>";
  }
  elseif(isset($confirm) == ""){
    echo "here7";
  $empty_confirm_password=  "<span class='error'>Please enter confirm password</span>";
  }

  elseif(!isset($data['password']) != isset($data['confirm_password'])) {
    echo "here8";
  $password_not_matched=  "<span class='error'>Password and confirm password does not match</span>";
  }

  else{
    echo "here9";
   return true;
  }
  return false;
}
function InsertRecords($formData)
{
  // $formData = array();
  // $data = $_SERVER['REQUEST_METHOD'];
  // if($data == 'POST')
  // {
    $formData = $_POST;
    validateData($formData);
    $query = "Insert Into users(first_name,last_name,email,password,role_id) Values('".$formData['firstname']."',
    '".$formData['lastname']."','".$formData['emailaddress']."','".$formData['password']."','".$formData['role_id']."' )";
    $isInserted = mysqli_query($conn,$query);
    if($isInserted){
      $last_inserted_user_id = mysqli_insert_id($conn);
      if(isset($_FILES['image_address'])){
        if (! is_dir ( '../Images/UserImages/'.$last_inserted_user_id )) {
          mkdir ('../Images/UserImages/'.$last_inserted_user_id, 0777, true );
        }
        if (! is_dir ('../Images/UserImages/'.$last_inserted_user_id.'/user_image' )) {
          mkdir ('../Images/UserImages/'.$last_inserted_user_id.'/user_image', 0777, true );
        }
        $imageServerAddress = '../Images/UserImages/'.$last_inserted_user_id.'/user_image/'.$_FILES['image_address']['name'];
        $isImageFileCopied = copy($_FILES['image_address']['tmp_name'], $imageServerAddress);
        if($isImageFileCopied){
          $queryImageUpdate = "update users set image_address = '".$imageServerAddress."' where id='".$last_inserted_user_id."' ";
          $isImageUpdated = mysqli_query($conn,$queryImageUpdate);
          if($isImageUpdated)
          {
            header("Location: index.php");
          }
    
          }
          else{
            echo "<script>alert('Image couold not be saved')</script>";        
          }
        }
    }
    else{
      echo "<script>alert('Data could not be inserted')</script>";        
    }
  // }      
}


?>

<?php
    include('../footer.php');
?>

<script>
  $('#id-input-file-1 , #id-input-file-2').ace_file_input({
    no_file:'No File ...',
    btn_choose:'Choose',
    btn_change:'Change',
    droppable:false,
    onchange:null,
    thumbnail:false //| true | large
    //whitelist:'gif|png|jpg|jpeg'
    //blacklist:'exe|php'
    //onchange:''
    //
  });
  $('#id-input-file-3').ace_file_input({
    style:'well',
    btn_choose:'Drop files here or click to choose',
    btn_change:null,
    no_icon:'icon-cloud-upload',
    droppable:true,
    thumbnail:'small'//large | fit
    //,icon_remove:null//set null, to hide remove/reset button
    /**,before_change:function(files, dropped) {
      //Check an example below
      //or examples/file-upload.html
      return true;
    }*/
    /**,before_remove : function() {
      return true;
    }*/
    ,
    preview_error : function(filename, error_code) {
      debugger;
      //name of the file that failed
      //error_code values
      //1 = 'FILE_LOAD_FAILED',
      //2 = 'IMAGE_LOAD_FAILED',
      //3 = 'THUMBNAIL_FAILED'
      //alert(error_code);
    }

  }).on('change', function(){
    //console.log($(this).data('ace_input_files'));
    //console.log($(this).data('ace_input_method'));
  });
  $(".chosen-selectt").chosen(); 
  $('#chosen-multiple-style').on('click', function(e){
    var target = $(e.target).find('input[type=radio]');
    var which = parseInt(target.val());
    if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
      else $('#form-field-select-4').removeClass('tag-input-style');
  });
  
</script>