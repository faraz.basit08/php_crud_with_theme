<?php
    
    include('../Db_Connectivity/connection.php');

    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
        // Fetching
        $query = "select * from users where id='".$_GET['id']."' ";
        $result = mysqli_query($conn,$query);
        $data = mysqli_fetch_assoc($result);
    }
  
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<form method="POST">
<div class="container-fluid">
<div class="form-group col-md-6">
    <label for="exampleInputfirstname">first name</label>

    <input type="hidden" class="form-control" id="exampleInputfirstname" name="user_id" value="<?= $data['id'] ?? ''?>" aria-describedby="firstname" placeholder="Enter first name">
    <input type="text" class="form-control" id="exampleInputfirstname" name="firstname" value="<?= $data['first_name'] ?? ''?>" aria-describedby="firstname" placeholder="Enter first name">
    
    <small id="firstname" class="form-text text-muted"></small>
  </div>
  <div class="form-group col-md-6">
    <label for="exampleInputlastname">last name</label>
    <input type="text" class="form-control" id="exampleInputlastname" name="lastname" value="<?= $data['last_name'] ?? ''?>" aria-describedby="lastname" placeholder="Enter last name">
    <small id="lastname" class="form-text text-muted"></small>
  </div>
  <div class="form-group col-md-6">
    <label for="exampleInputEmail">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="emailaddress" value="<?= $data['email'] ?? ''?>" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted"></small>
  </div>
  <div class="form-group col-md-6">
    <label for="exampleInputPassword">Password</label>
    <input type="password" class="form-control" name="password" id="exampleInputPassword1" value="<?php echo $data['password'] ?? ''?>" placeholder="Password">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  </div>
  
</form>
<?php


    // echo "<pre>";
    // print_r($_REQUEST);
    // echo "<br>";
    // die("Code is stopped here...");
    $formData = array();    
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {

        $formData = $_POST;
        // $query = "Insert Into users(first_name,last_name,email,password) Values('".$formData['firstname']."',
        // '".$formData['lastname']."','".$formData['emailaddress']."','".$formData['password']."'
        // )";

        $query = "update users set first_name = '".$formData['firstname']."' , last_name = '".$formData['lastname']."',
        email = '".$formData['emailaddress']."' , password = '".$formData['password']."' where id='".$formData['user_id']."' ";

        $isInserted = mysqli_query($conn,$query);
        if($isInserted)
        {
          header("Location: index.php");
        }
        else
        {
          echo "<script>alert('Data could not be inserted')</script>";
        }

      }
?>
 
</body>
</html>