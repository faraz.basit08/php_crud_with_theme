<?php
    include('../Db_Connectivity/connection.php');
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
        // Fetching
        $query = "select * from users where id='".$_GET['id']."' ";
        $result = mysqli_query($conn,$query);
        $data = mysqli_fetch_assoc($result);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>
<body>
<form >
<div class="container-fluid">
<div class="form-group col-md-6">
    <label for="exampleInputfirstname">first name</label>

    <input type="text" disabled class="form-control" id="exampleInputfirstname" name="firstname" value="<?= $data['first_name'] ?? ''?>" aria-describedby="firstname" placeholder="Enter first name">
    
    <small id="firstname" class="form-text text-muted"></small>
  </div>
  <div class="form-group col-md-6">
    <label for="exampleInputlastname">last name</label>
    <input type="text" disabled class="form-control" id="exampleInputlastname" name="lastname" value="<?= $data['last_name'] ?? ''?>" aria-describedby="lastname" placeholder="Enter last name">
    <small id="lastname" class="form-text text-muted"></small>
  </div>
  <div class="form-group col-md-6">
    <label for="exampleInputEmail">Email address</label>
    <input type="email" disabled class="form-control" id="exampleInputEmail1" name="emailaddress" value="<?= $data['email'] ?? ''?>" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted"></small>
  </div>
  <div class="form-group col-md-6">
    <label for="exampleInputPassword">Password</label>
    <input type="password" disabled class="form-control" name="password" id="exampleInputPassword1" value="<?php echo $data['password'] ?? ''?>" placeholder="Password">

  
</form>

</body>
</html>