<?php
    ob_start();
    session_start();
    
    if(!isset($_SESSION['username']))
    {
      header('location: ../Login/login.php');
    }

    include('../Db_Connectivity/connection.php');
    include('../master_layoout.php');

    // Fetching
    $query = "select * from roles ORDER by id DESC ";
    $result = mysqli_query($conn,$query);
    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
    // echo "<pre>";
    // print_r($data);

?>

<a href="insert.php" class="btn btn-secondary">Add New</a>

  <table class="table table-hover ">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Role Name</th>
      <th scope="col" colspan="3">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php 
      for($i = 0; $i<count($data); $i++)
      {
        ?>
        <tr>
          <td scope="row"><?php echo $i+1;?></td>
          <td scope="row"><?php echo $data[$i]['role_name'] ?? ''?></td>
          <td><a href="update.php?id=<?= $data[$i]['id']?>" class="btn btn-success" >Edit</a> </td>
          <!-- <td><a href="delete.php?id=<?= $data[$i]['id']?>" class="btn btn-danger" >Delete</a> </td> -->
          <td><button onclick="deleteRole(<?= $data[$i]['id']?>)" class="btn btn-danger" >Delete</button> </td>
          <td><a href="view.php?id=<?= $data[$i]['id']?>" class="btn btn-primary" >View</a> </td>
        </tr>
     <?php
      }
    ?>

      
      <!-- <td>
        <?php 
            // if(isset($formData['firstname']))
            // {
            //     echo $formData['firstname'];
            // }
        ?>
      </td>
      <td>
        <?php 
            // if(isset($formData['lastname']))
            // {
            //     echo $formData['lastname'];
            // }
        ?>
      </td>
      <td><?= $formData['emailaddress'] ?? ''?></td>
      <td><?= $formData['password'] ?? ''?></td> -->
    <!-- </tr> -->
  </tbody>
</table>
<script>
function deleteRole(id)
{
  event.preventDefault(); // prevent form submit
  var form = event.target.form; // storing the form
          swal({
    title: "Are you sure?",
    text: "User will be deleted.",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete role!",
    cancelButtonText: "No",
    closeOnConfirm: false,
    closeOnCancel: false
  },
  function(isConfirm){
    if (isConfirm) {
      var request = $.ajax({
        url: "delete.php",
        type: "GET",
        data: {id : id},
        json: "html"
      });
      

      request.done(function(msg) {
        if(msg.is_deleted == 1)
        {
          swal("Deleted", "Role Deleted", "success");
          location.reload();
        }
        if(msg.is_deleted == 0)
        {
          swal("Not Deleted", "Role could not be deleted", "error");
        }
      });

      request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
      });

    } else {
      swal("Cancelled", "Role did not deleted", "error");
    }
  });
}
</script>

<?php
    session_abort();
    include('../footer.php');
?>